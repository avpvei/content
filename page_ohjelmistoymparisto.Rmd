---
title: "Ohjelmistoympäristön asennus ja määrittely"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: yes
    code_folding: show
---


Sisältö pilkottu kahdelle eri sivulle, ks. yläpalkin otsikon **Ohjelmistoympäristö** alta!